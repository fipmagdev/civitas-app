import React from 'react';
import {View, Image } from 'react-native';
import{ImagemLogo, BackGround, ImagemFooter} from './style'

export default function PagLogo() {
  return (
    <BackGround>
      <ImagemLogo source={require('../../assets/civitas-logo200.png')} />
      <ImagemFooter source={require('../../assets/logo-fipdev66.png')} />
    </BackGround>
  );
}


