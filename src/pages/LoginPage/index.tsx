import React from 'react';
import { Text, View, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LogButton from '../../Components/LogButton/index'
import Email from '../../Components/Email';
import Senha from '../../Components/Senha';
import { Container1, Container2, Texto1, Texto2, Letreiro, LoginLogo, Form, Title, Bloco } from './style';
import LoginWith from '../../Components/LoginWith';

export default function LoginPage() {
    return (
        <View>
            <Container1>
                <Letreiro>
                    <Texto1>Bem-Vindo,</Texto1>
                    <Texto2>novamente</Texto2>
                </Letreiro>
                <LoginLogo source={require('../../assets/civitas-logo200.png')} />
            </Container1>

            <Container2>

                <Form>
                    <Title>Entrar</Title>
                    <Email />
                    <Senha />
                    <LogButton />
                </Form>
                <Bloco>
                <LoginWith />
                </Bloco>
            </Container2>
        </View>
    );
}