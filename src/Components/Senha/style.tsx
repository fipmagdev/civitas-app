import React from 'react';
import { StyleSheet, Text, Image } from 'react-native';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const InputTexto = styled.TextInput`
border-radius:71;
background-color:white
width:344px;
height:50px;
padding-left:25px;
padding-right:24px;
filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
font-size:24px;
font-style:italic;
`